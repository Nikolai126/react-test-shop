import React from 'react';
import { Route, Routes } from 'react-router-dom';

import Header from './components/Header/Header';
import { MainPage, BasketPage, FavoritePage } from './pages';

function App () {
  return (
      <>
          <Header />
          <Routes>
              <Route path="/basket" element={<BasketPage/>}></Route>
              <Route path="/favorites" element={<FavoritePage />}></Route>
              <Route path="/" element={<MainPage />}></Route>
          </Routes>
      </>
  )
}

export default App;
