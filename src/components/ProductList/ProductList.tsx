import { IProduct } from '../../models';

import Product from '../Product/Product';
// import { MySkeleton } from '../CustomSkeleton/MySkeleton';

// import { useGetProductsQuery } from '../../store/product/product.api';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import { MAIN_PATH, BASKET_PATH, FAVORITE_PATH } from '../../constants';
import { useEffect } from 'react';
import { fetchProducts } from '../../store/asyncActions/getAllProducts';

function ProductList (props: { path: string }) {
  // const { data, isLoading, error } = useGetProductsQuery(30);
  const data = useTypedSelector(state => state.all);
  const basket = useTypedSelector(state => state.basket);
  const favorite = useTypedSelector(state => state.favorite);
  const filtered = useTypedSelector(state => state.filtered);

  useEffect(() => {
    fetchProducts();
  });

  // function getFilteredProducts () {
  //   return data.products.filter((product: { brand: string }) => filtered.includes(product.brand));
  // }

  // if (error != null) {
  //   return (
  //           <div>Error, please, try again</div>
  //   )
  // }

  // if (isLoading) {
  //   return ()
  // }

  return (
        <>
            {(
                <>
                    { props.path === MAIN_PATH && (filtered.length === 0) && data?.map((product: IProduct) => <Product product={product} key={product.id}/>) }
                    {/* { props.path === MAIN_PATH && (filtered.length > 0) && getFilteredProducts().map((product: IProduct) => <Product product={product} key={product.id}/>) } */}
                    { props.path === BASKET_PATH && basket?.map((product: IProduct) => <Product product={product} key={product.id}/>) }
                    { props.path === FAVORITE_PATH && favorite?.map((product: IProduct) => <Product product={product} key={product.id}/>) }
                </>
                )}
        </>
  )
}

export default ProductList;
