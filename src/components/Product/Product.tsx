// import { useActions } from '../../hooks/useActions';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import { useDispatch } from 'react-redux';

import { IProduct } from '../../models';
import { ADD_PRODUCT_TO_FAVORITE, REMOVE_PRODUCT_BASKET, REMOVE_PRODUCT_FAVORITE } from '../../store/constants';

import { Card, IconButton } from '@mui/material';
import { green, red } from '@mui/material/colors';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Typography from '@mui/material/Typography';
import FavoriteIcon from '@mui/icons-material/Favorite';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import Grid from '@mui/material/Grid';
import ShoppingCartCheckoutIcon from '@mui/icons-material/ShoppingCartCheckout';

interface ProductProps {
  product: IProduct
}

function Product ({ product }: ProductProps) {
  // const { addItemToBasket, removeItemFromBasket, addItemToFavorite, removeItemFromFavorite } = useActions();
  // const { basket, favorite } = useTypedSelector(state => state);
  const dispatch = useDispatch();

  const basket = useTypedSelector(state => state.basket);
  const favorite = useTypedSelector(state => state.favorite);

  const isExistInBasket = basket.some((item: IProduct) => item.id === product.id);
  const isExistInFavorites = favorite.some((item: IProduct) => item.id === product.id);

  function addProductToFavorite (product: IProduct) {
    dispatch({ type: ADD_PRODUCT_TO_FAVORITE, payload: product });
  }

  function addProductToBasket (product: IProduct) {
    dispatch({ type: ADD_PRODUCT_TO_FAVORITE, payload: product });
  }

  function removeProductFromFavorite (productId: number) {
    dispatch({ type: REMOVE_PRODUCT_FAVORITE, payload: product });
  }

  function removeProductFromBasket (productId: number) {
    dispatch({ type: REMOVE_PRODUCT_BASKET, payload: product });
  }

  return (
    <Grid item xs={12} md={6} lg={4} xl={3} sx={{ justifyContent: 'center' }}>
      <Card elevation={5} sx={{ minWidth: '220px', marginBottom: '20px', maxWidth: '250px', marginLeft: 'auto', marginRight: 'auto' }}>
        <CardMedia
          component="img"
          height="194"
          image={product.images[0]}
          alt="Paella dish"
        />
        <CardContent>
          <Typography variant="body2" color="text.secondary">
            <h6>{product.title}</h6>
          </Typography>
          <Typography variant="body2" color="text.secondary">
            <span >{product.price}$</span>
          </Typography>
        </CardContent>
        <CardActions disableSpacing>
          <IconButton aria-label="add to favorites" sx={{ outline: 'none !important' }}>
            { !isExistInFavorites
              ? <FavoriteIcon onClick={() => !isExistInFavorites && addProductToFavorite(product)}/>
              : <FavoriteIcon sx={{ color: red[500] }} onClick={() => isExistInFavorites && removeProductFromFavorite(product.id)}/>
            }
          </IconButton>
          <IconButton aria-label="add to basket" sx={{ outline: 'none !important' }}>
            { !isExistInBasket
              ? <AddShoppingCartIcon onClick={() => !isExistInBasket && addProductToBasket(product)} />
              : <ShoppingCartCheckoutIcon sx={{ color: green[500] }} onClick={() => isExistInBasket && removeProductFromBasket(product.id)}/>
            }
          </IconButton>
        </CardActions>
      </Card>
    </Grid>
  )
}

export default Product;
