import * as React from 'react';

import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';
import Skeleton from '@mui/material/Skeleton';
import Card from '@mui/material/Card';

export const MySkeleton = () => {
  return (
      <Grid container>
        <Grid item xs={12} md={6} lg={4} xl={3}>
          <Card sx={{ maxWidth: 250, m: 2, minWidth: 220 }}>
            <Skeleton sx={{ height: 190 }} animation="wave" variant="rectangular" />
            <CardContent>
              <>
                <Skeleton animation="wave" height={10} style={{ marginBottom: 6 }} />
                <Skeleton animation="wave" height={10} width="80%" />
              </>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={4} xl={3}>
          <Card sx={{ maxWidth: 250, m: 2, minWidth: 220 }}>
            <Skeleton sx={{ height: 190 }} animation="wave" variant="rectangular" />
            <CardContent>
              <React.Fragment>
                <Skeleton animation="wave" height={10} style={{ marginBottom: 6 }} />
                <Skeleton animation="wave" height={10} width="80%" />
              </React.Fragment>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={4} xl={3}>
          <Card sx={{ maxWidth: 250, m: 2, minWidth: 220 }}>
            <Skeleton sx={{ height: 190 }} animation="wave" variant="rectangular" />
            <CardContent>
              <React.Fragment>
                <Skeleton animation="wave" height={10} style={{ marginBottom: 6 }} />
                <Skeleton animation="wave" height={10} width="80%" />
              </React.Fragment>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={4} xl={3}>
          <Card sx={{ maxWidth: 250, m: 2, minWidth: 220 }}>
            <Skeleton sx={{ height: 190 }} animation="wave" variant="rectangular" />
            <CardContent>
              <React.Fragment>
                <Skeleton animation="wave" height={10} style={{ marginBottom: 6 }} />
                <Skeleton animation="wave" height={10} width="80%" />
              </React.Fragment>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={4} xl={3}>
          <Card sx={{ maxWidth: 250, m: 2, minWidth: 220 }}>
            <Skeleton sx={{ height: 190 }} animation="wave" variant="rectangular" />
            <CardContent>
              <React.Fragment>
                <Skeleton animation="wave" height={10} style={{ marginBottom: 6 }} />
                <Skeleton animation="wave" height={10} width="80%" />
              </React.Fragment>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={4} xl={3}>
          <Card sx={{ maxWidth: 250, m: 2, minWidth: 220 }}>
            <Skeleton sx={{ height: 190 }} animation="wave" variant="rectangular" />
            <CardContent>
              <React.Fragment>
                <Skeleton animation="wave" height={10} style={{ marginBottom: 6 }} />
                <Skeleton animation="wave" height={10} width="80%" />
              </React.Fragment>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={4} xl={3}>
          <Card sx={{ maxWidth: 250, m: 2, minWidth: 220 }}>
            <Skeleton sx={{ height: 190 }} animation="wave" variant="rectangular" />
            <CardContent>
              <React.Fragment>
                <Skeleton animation="wave" height={10} style={{ marginBottom: 6 }} />
                <Skeleton animation="wave" height={10} width="80%" />
              </React.Fragment>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={4} xl={3}>
          <Card sx={{ maxWidth: 250, m: 2, minWidth: 220 }}>
            <Skeleton sx={{ height: 190 }} animation="wave" variant="rectangular" />
            <CardContent>
              <React.Fragment>
                <Skeleton animation="wave" height={10} style={{ marginBottom: 6 }} />
                <Skeleton animation="wave" height={10} width="80%" />
              </React.Fragment>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={4} xl={3}>
          <Card sx={{ maxWidth: 250, m: 2, minWidth: 220 }}>
            <Skeleton sx={{ height: 190 }} animation="wave" variant="rectangular" />
            <CardContent>
              <React.Fragment>
                <Skeleton animation="wave" height={10} style={{ marginBottom: 6 }} />
                <Skeleton animation="wave" height={10} width="80%" />
              </React.Fragment>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={4} xl={3}>
          <Card sx={{ maxWidth: 250, m: 2, minWidth: 220 }}>
            <Skeleton sx={{ height: 190 }} animation="wave" variant="rectangular" />
            <CardContent>
              <React.Fragment>
                <Skeleton animation="wave" height={10} style={{ marginBottom: 6 }} />
                <Skeleton animation="wave" height={10} width="80%" />
              </React.Fragment>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={4} xl={3}>
          <Card sx={{ maxWidth: 250, m: 2, minWidth: 220 }}>
            <Skeleton sx={{ height: 190 }} animation="wave" variant="rectangular" />
            <CardContent>
              <React.Fragment>
                <Skeleton animation="wave" height={10} style={{ marginBottom: 6 }} />
                <Skeleton animation="wave" height={10} width="80%" />
              </React.Fragment>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={4} xl={3}>
          <Card sx={{ maxWidth: 250, m: 2, minWidth: 220 }}>
            <Skeleton sx={{ height: 190 }} animation="wave" variant="rectangular" />
            <CardContent>
              <React.Fragment>
                <Skeleton animation="wave" height={10} style={{ marginBottom: 6 }} />
                <Skeleton animation="wave" height={10} width="80%" />
              </React.Fragment>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
  )
}
