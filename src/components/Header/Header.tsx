import React from 'react';

import { Link } from 'react-router-dom';

import { useTypedSelector } from '../../hooks/useTypedSelector';

import { Box, createTheme } from '@mui/material';
import Typography from '@mui/material/Typography'
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import InputBase from '@mui/material/InputBase';
import Badge from '@mui/material/Badge';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import MoreIcon from '@mui/icons-material/MoreVert';
import { alpha, styled, ThemeProvider } from '@mui/material/styles';
import ShoppingBasketIcon from '@mui/icons-material/ShoppingBasket';
import FavoriteIcon from '@mui/icons-material/Favorite';
import HomeIcon from '@mui/icons-material/Home';
import ListItemText from '@mui/material/ListItemText';

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25)
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(3),
    width: 'auto'
  }
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center'
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch'
    }
  }
}));

export default function Header () {
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] =
    React.useState<null | HTMLElement>(null);
  const { basket, favorite } = useTypedSelector(state => state);

  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMobileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const theme = createTheme({
    typography: {
      htmlFontSize: 10,
      fontFamily: 'BlinkMacSystemFont'
    }
  })

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right'
      }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right'
      }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <Link to={'/'} style={{ textDecoration: 'none' }}>
        <MenuItem>
            <IconButton size="large" sx={{ outline: 'none !important' }}>
              <HomeIcon fontSize="medium" sx={{ color: 'black' }}/>
            </IconButton>
          <ListItemText sx={{ color: 'black' }}>Home</ListItemText>
        </MenuItem>
      </Link>
      <Link to={'/favorites'} style={{ textDecoration: 'none' }}>
        <MenuItem>
            <IconButton size="large" sx={{ outline: 'none !important' }}>
              <Badge badgeContent={favorite.length} color="error">
                <FavoriteIcon sx={{ color: 'black' }}/>
              </Badge>
            </IconButton>
          <ListItemText sx={{ color: 'black' }}>Favorites</ListItemText>
        </MenuItem>
      </Link>
      <Link to={'/basket'} style={{ textDecoration: 'none' }}>
        <MenuItem>
          <IconButton size="large"
            sx={{ outline: 'none !important' }}
          >
            <Badge badgeContent={basket.length} color="error">
              <ShoppingBasketIcon sx={{ color: 'black' }}/>
            </Badge>
          </IconButton>
          <ListItemText sx={{ color: 'black' }}>Basket</ListItemText>
        </MenuItem>
      </Link>
    </Menu>
  );

  return (
    <Box sx={{ flexGrow: 1, paddingBottom: '70px' }}>
      <AppBar position="fixed" sx={{ backgroundColor: '#3F3347' }}>
        <Toolbar>
          <ThemeProvider theme={theme}>
            <Link to={'/'} style={{ textDecoration: 'none' }}>
              <Typography
                variant="h6"
                noWrap
                component="div"
                sx={{ display: { xs: 'none', sm: 'block' }, color: 'white' }}
              >
                React shop
              </Typography>
            </Link>
          </ThemeProvider>
          <Search>
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase
              placeholder="Search…"
              inputProps={{ 'aria-label': 'search' }}
            />
          </Search>
          <Box sx={{ flexGrow: 1 }} />
          <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
            <Link to={'/'}>
              <IconButton size="large" sx={{ outline: 'none !important' }}>
                <HomeIcon fontSize="medium" sx={{ color: 'white' }}/>
              </IconButton>
            </Link>
            <Link to={'/favorites'}>
              <IconButton size="large" sx={{ outline: 'none !important' }}>
                <Badge badgeContent={favorite.length} color="error">
                  <FavoriteIcon sx={{ color: 'white' }}/>
                </Badge>
              </IconButton>
            </Link>
            <Link to={'/basket'}>
              <IconButton size="large" sx={{ outline: 'none !important' }}>
                <Badge badgeContent={basket.length} color="error">
                  <ShoppingBasketIcon sx={{ color: 'white' }}/>
                </Badge>
              </IconButton>
            </Link>
          </Box>
          <Box sx={{ display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              size="large"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              sx={{ outline: 'none !important' }}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </Box>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
    </Box>
  );
}
