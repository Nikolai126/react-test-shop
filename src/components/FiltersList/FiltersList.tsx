import React, { useEffect, useState } from 'react';
import axios from 'axios';

import { useActions } from '../../hooks/useActions';
import { useTypedSelector } from '../../hooks/useTypedSelector';

import { IProduct } from '../../models';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Checkbox from '@mui/material/Checkbox';
import Box from '@mui/material/Box'
import { FormControlLabel } from '@mui/material';

function FiltersList () {
  const [brands, setBrands] = useState<string[]>([]);
  const { takeFilter, removeFilter } = useActions();
  const filtered = useTypedSelector(state => state.filtered);
  const [expanded, setExpanded] = useState<string | false>(false);

  const handleChange = (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => {
    setExpanded(isExpanded ? panel : false);
  };

  async function getProducts () {
    const res: IProduct[] = await axios.get('https://dummyjson.com/products').then((res) => {
      return res.data.products;
    });
    return Array.from(new Set(res.map(product => product.brand)));
  }

  useEffect(() => {
    getProducts().then(res => {
      setBrands(res);
    })
  }, [getProducts])

  return (
    <Box>
        <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
          >
            <Typography sx={{ width: '33%', flexShrink: 0 }}>
              Brands
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            {brands.map((brand) =>
              <FormControlLabel
                key={brands.indexOf(brand)}
                label={brand}
                control={
                  <Checkbox
                    checked={filtered.includes(brand)}
                    onChange={(event) => { event.target.checked ? takeFilter(brand) : removeFilter(brand) }}
                  />
                }
              />
            )}
          </AccordionDetails>
        </Accordion>
      </Box>
  )
}

export default FiltersList;
