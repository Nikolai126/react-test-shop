import React from 'react';

import { BASKET_PATH } from '../constants';
import { useTypedSelector } from '../hooks/useTypedSelector';

import ProductList from '../components/ProductList/ProductList';

import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import { Box, Container } from '@mui/material';
import ProductionQuantityLimitsIcon from '@mui/icons-material/ProductionQuantityLimits';
import { IProduct } from '../models';

const BasketPage = () => {
  const basket = useTypedSelector(state => state.basket)
  const isBasketEmpty = basket.length;

  function getTotalCheck () {
    return (basket?.map((product: IProduct) => product.price)).reduce((acc: number, currentProduct: number) => acc + currentProduct);
  }

  return (
        <>
            {
                !isBasketEmpty
                  ? <Container sx={{ paddingTop: '20%', fontSize: '28px', textAlign: 'center' }}>
                      <Box>
                        <ProductionQuantityLimitsIcon fontSize="large" width="100%"/>
                      </Box>
                      Basket is empty
                    </Container>
                  : <>
                      <Typography sx={{ marginLeft: 'auto', marginRight: 'auto', fontSize: '28px', textAlign: 'center' }}>Total check of basket: {getTotalCheck()} $</Typography>
                      <hr style={{ width: '50%', marginLeft: 'auto', marginRight: 'auto' }}/>
                      <Grid container xs={12} md={8} lg={9} spacing={3} sx={{ paddingTop: '20px', marginLeft: 'auto', marginRight: 'auto' }}>
                        <ProductList path={BASKET_PATH}/>
                      </Grid>
                    </>

            }

        </>
  )
}

export default BasketPage
