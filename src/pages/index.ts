export { default as BasketPage } from './BasketPage';
export { default as MainPage } from './MainPage';
export { default as FavoritePage } from './FavoritePage';
