import React, { useEffect } from 'react';

import FiltersList from '../components/FiltersList/FiltersList';
import ProductList from '../components/ProductList/ProductList';
import { MAIN_PATH } from '../constants';

import Grid from '@mui/material/Grid';
import { fetchProducts } from '../store/asyncActions/getAllProducts';
import { useDispatch } from 'react-redux';

const MainPage = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProducts());
  })

  return (
    <Grid container>
      <Grid item xs={12} md={4} lg={3} sx={{ padding: '20px 30px 0 30px' }}>
        <FiltersList />
      </Grid>
      <Grid container xs={12} md={8} lg={9} spacing={3} sx={{ paddingTop: '20px' }}>
        <ProductList path={MAIN_PATH}/>
      </Grid>
    </Grid>
  )
}

export default MainPage;
