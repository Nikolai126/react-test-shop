import React from 'react';

import ProductList from '../components/ProductList/ProductList';
import { FAVORITE_PATH } from '../constants';

import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';

const FavoritePage = () => {
  return (
    <>
      <Typography sx={{ marginLeft: 'auto', marginRight: 'auto', fontSize: '28px', textAlign: 'center' }}>Your favorite products</Typography>
      <hr style={{ width: '50%', marginLeft: 'auto', marginRight: 'auto' }}/>
        <Grid container xs={12} md={8} lg={9} spacing={3} sx={{ paddingTop: '20px', marginLeft: 'auto', marginRight: 'auto' }}>
          <ProductList path={FAVORITE_PATH}/>
        </Grid>
    </>
  )
}

export default FavoritePage
