import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const initialState: string[] = [];

export const filterSlice = createSlice({
  name: 'filter',
  initialState,
  reducers: {
    takeFilter: (state, action: PayloadAction<string>) => {
      state.push(action.payload);
    },
    removeFilter: (state, action: PayloadAction<string>) => {
      return state.filter(item => action.payload !== item);
    }
  }
});

export const filterReducer = filterSlice.reducer;
export const filterActions = filterSlice.actions;
