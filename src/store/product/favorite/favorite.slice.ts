import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { IProduct } from '../../../models';

const initialState: IProduct[] = [];

export const favoriteSlice = createSlice({
  name: 'favorite',
  initialState,
  reducers: {
    addItemToFavorite: (state, action: PayloadAction<IProduct>) => {
      state.push(action.payload);
    },
    removeItemFromFavorite: (state, action: PayloadAction<{ id: number }>) => {
      return state.filter(item => action.payload.id !== item.id);
    }
  }
});

export const favoriteReducer = favoriteSlice.reducer;
export const favoriteActions = favoriteSlice.actions;
