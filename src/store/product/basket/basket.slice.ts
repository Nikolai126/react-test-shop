import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { IProduct } from '../../../models';

const initialState: IProduct[] = [];

export const basketSlice = createSlice({
  name: 'basket',
  initialState,
  reducers: {
    addItemToBasket: (state, action: PayloadAction<IProduct>) => {
      state.push(action.payload);
    },
    removeItemFromBasket: (state, action: PayloadAction<{ id: number }>) => {
      return state.filter(item => action.payload.id !== item.id);
    }
  }
});

export const basketReducer = basketSlice.reducer;
export const basketActions = basketSlice.actions;
