import {
  ADD_ALL_PRODUCTS,
  ADD_PRODUCT_TO_BASKET,
  ADD_PRODUCT_TO_FAVORITE, REMOVE_FILTER_MAINPAGE,
  REMOVE_PRODUCT_BASKET,
  REMOVE_PRODUCT_FAVORITE,
  TAKE_FILTER_MAINPAGE
} from './constants';

import { IProduct } from '../models';

interface IState {
  favorite: IProduct[]
  basket: IProduct[]
  all: IProduct[]
  filtered: IProduct[]
}

const defaultState: IState = { favorite: [], basket: [], all: [], filtered: [] };

export const customReducer = (state = defaultState, action: { type: string, payload: any }) => {
  switch (action.type) {
    case ADD_ALL_PRODUCTS:
      return { ...state, products: [...state.all, ...action.payload] }
    case ADD_PRODUCT_TO_FAVORITE:
      return { ...state, products: [...state.favorite, ...action.payload] }
    case ADD_PRODUCT_TO_BASKET:
      return { ...state, products: [...state.basket, ...action.payload] }
    case REMOVE_PRODUCT_FAVORITE:
      return { ...state, products: state.favorite.filter(product => product.id !== action.payload) }
    case REMOVE_PRODUCT_BASKET:
      return { ...state, products: state.basket.filter(product => product.id !== action.payload) }
    case TAKE_FILTER_MAINPAGE:
      return { ...state, products: [...state.filtered, ...action.payload] }
    case REMOVE_FILTER_MAINPAGE:
      return { ...state, products: state.basket.filter(product => product.id !== action.payload) }
    default:
      return state
  }
}
