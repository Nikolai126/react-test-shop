import axios from 'axios';
import { useDispatch } from 'react-redux';

import { ADD_ALL_PRODUCTS } from '../constants';

export const fetchProducts = async () => {
  const dispatch = useDispatch();
  await axios.get('https://dummyjson.com/products').then(res => {
    return dispatch({ type: ADD_ALL_PRODUCTS, payload: res.data.products });
  });
}
