// import { configureStore } from '@reduxjs/toolkit';
//
// import { productApi } from './product/product.api';
// import { basketReducer } from './product/basket/basket.slice';
// import { favoriteReducer } from './product/favorite/favorite.slice';
// import { filterReducer } from './product/filter/filter.slice';
//
// export const store = configureStore({
//   reducer: { [productApi.reducerPath]: productApi.reducer, basket: basketReducer, favorite: favoriteReducer, filtered: filterReducer }
// })
//

import { applyMiddleware } from '@reduxjs/toolkit';
import { legacy_createStore as createStore } from 'redux';
import { customReducer } from './customReducer';
import thunk from 'redux-thunk';

export const store = createStore(customReducer, applyMiddleware(thunk));

// export const store = configureStore({
//   reducer: customReducer,
//   middleware: getDefaultMiddleware => {
//   }
// });
//
export type TypeRootState = ReturnType<typeof store.getState>;
