import { useDispatch } from 'react-redux';
import { bindActionCreators } from '@reduxjs/toolkit';

import { favoriteActions } from '../store/product/favorite/favorite.slice';
import { basketActions } from '../store/product/basket/basket.slice';
import { filterActions } from '../store/product/filter/filter.slice';

const allActions = {
  ...basketActions,
  ...favoriteActions,
  ...filterActions
}

export const useActions = () => {
  const dispatch = useDispatch();
  return bindActionCreators(allActions, dispatch);
}
